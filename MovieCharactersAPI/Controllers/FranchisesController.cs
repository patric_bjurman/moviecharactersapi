﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Character;
using MovieCharactersAPI.Models.DTOs.Franchise;
using MovieCharactersAPI.Models.DTOs.Movie;
using System.Net.Mime;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieCharactersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all franchises from the database.
        /// </summary>
        /// /// <response code = "200">When all franchises is returned.</response>
        /// <returns>Returns a list of all franchises in the database.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchises.ToListAsync());
        }

        /// <summary>
        /// Get a specific franchise from the database.
        /// Submit franchise id.
        /// </summary>
        /// <param name="id">The id for the franchise</param>
        /// <response code = "200">If franchise is found.</response>
        /// <response code = "404">If no franchise with that id exists.</response>
        /// <returns>Returns a specific franchise from the database.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Get all movies in a franchise.
        /// Submit franchise id.
        /// </summary>
        /// <param name="franchiseId">The id for the franchise</param>
        /// <response code = "200">When all movies in franchises is returned.</response>
        /// <response code = "404">If no franchise with that id exists. Or if no movies in franchise.</response>
        /// <returns>Returns all movies in a franchise.</returns>
        [HttpGet("{franchiseId}/GetAllMoviesInFranchise")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMoviesInFranchise(int franchiseId)
        {
            var franchise = await _context.Franchises.FindAsync(franchiseId);
            if (franchise == null)
                return NotFound();

            FranchiseReadDTO domainFranchise = _mapper.Map<FranchiseReadDTO>(franchise);

            var moviesInFranchise = _context.Movies.Where(m => m.FranchiseId == domainFranchise.FranchiseId);

            if (moviesInFranchise.Count() == 0)
                return NotFound();

            return _mapper.Map<List<MovieReadDTO>>(await moviesInFranchise.ToListAsync());
        }

        /// <summary>
        /// Get all characters in a franchise.
        /// Submit franchise id.
        /// </summary>
        /// <param name="franchiseId">The id for the franchise</param>
        /// <response code = "200">When all characters in franchises is returned.</response>
        /// <response code = "404">If no franchise with that id exists. Or if no characters in franchise.</response>
        /// <returns>Returns all characters in a franchise.</returns>
        [HttpGet("{franchiseId}/GetAllCharactersInFranchise")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharactersInFranchise(int franchiseId)
        {
            var franchise = await _context.Franchises.FindAsync(franchiseId);
            if (franchise == null)
                return NotFound();

            FranchiseReadDTO domainFranchise = _mapper.Map<FranchiseReadDTO>(franchise);

            var charactersInFranchise = (from character in _context.Characters
                         from movieChar in _context.MovieCharacter
                         from movie in _context.Movies
                         where character.CharacterId == movieChar.CharacterId
                         where movieChar.MovieId == movie.MovieId
                         where movie.FranchiseId == domainFranchise.FranchiseId
                         select character).Distinct();

            if (charactersInFranchise.Count() == 0)
                return NotFound();

            return _mapper.Map<List<CharacterReadDTO>>(await charactersInFranchise.ToListAsync());
        }

        /// <summary>
        /// Update a specific franchise.
        /// </summary>
        /// <param name="id">The id for the franchise</param>
        /// <param name="franchise">The franchise in JSON</param>
        /// <response code = "204">If franchise is successfully updated.</response>
        /// <response code = "404">If no franchise with that id exists.</response>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchise)
        {
            if (id != franchise.FranchiseId)
            {
                return BadRequest();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);
            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add a new franchise to the database.
        /// </summary>
        /// <param name="franchise">The franchise to add to the database.</param>
        /// <response code = "201">When a franchise is added to the database.</response>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseCreateDTO>> PostFranchise(FranchiseCreateDTO franchise)
        {
            Franchise domainFran = _mapper.Map<Franchise>(franchise);
            _context.Franchises.Add(domainFran);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = domainFran.FranchiseId }, domainFran);
        }

        /// <summary>
        /// Add movie to franchise.
        /// Submit movie id.
        /// Submit franchise id.
        /// </summary>
        /// <param name="movieId">The id for the movie.</param>
        /// <param name="franchiseId">The id for the franchise.</param>
        /// <response code = "204">When a movie is added to a franchise.</response>
        /// <response code = "404">If no movie or franchise with that id exists.</response>
        /// <returns></returns>
        [HttpPost("{movieId}, {franchiseId}")]
        public async Task<IActionResult> AddMovieToFranchise(int movieId, int franchiseId)
        {
            var movie = await _context.Movies.FindAsync(movieId);
            var franchise = await _context.Franchises.FindAsync(franchiseId);
            if (movie == null || franchise == null)
                return NotFound();

            movie.FranchiseId = franchise.FranchiseId;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Delete a franchise from the database.
        /// Submit franchise id.
        /// </summary>
        /// <param name="id">The id for the franchise.</param>
        /// <response code = "204">When a franchise is successfully deleted.</response>
        /// <response code = "404">If no franchise with that id exists.</response>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Delete movie from franchise.
        /// Submit movie id.
        /// </summary>
        /// <param name="movieId">The id for the movie.</param>
        /// <response code = "204">When a movie is successfully deleted from a franchise.</response>
        /// <response code = "404">If no movie with that id exists.</response>
        /// <returns></returns>
        [HttpDelete("{movieId}/DeleteMovieFromFranchise")]
        public async Task<IActionResult> DeleteMovieFromFranchise(int movieId)
        {
            var movie = await _context.Movies.FindAsync(movieId);
            if (movie == null)
                return NotFound();

            movie.FranchiseId = null;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.FranchiseId == id);
        }
    }
}
