﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Movie;
using System.Net.Mime;
using MovieCharactersAPI.Models.DTOs.Character;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MovieCharactersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all movies from the database.
        /// </summary>
        /// <response code = "200">When all movies is returned.</response>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _context.Movies.ToListAsync());
        }

        /// <summary>
        /// Get a specific movie from the database.
        /// Submit movie id.
        /// </summary>
        /// <param name="id">The id for the movie.</param>
        /// <response code = "200">When the movie is returned.</response>
        /// <response code = "404">If no movie with that id exists.</response>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Get all characters in a movie.
        /// Submit movie id.
        /// </summary>
        /// <param name="movieId">The id for the movie.</param>
        /// <response code = "200">When all characters in a movie is returned.</response>
        /// <response code = "404">If no movie with that id exists. Or if no characters are in that movie.</response>
        /// <returns></returns>
        [HttpGet("{movieId}/GetCharactersInMovie")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharactersInMovie(int movieId)
        {
            var movie = await _context.Movies.FindAsync(movieId);
            if (movie == null)
                return NotFound();

            // Find all movieCharacters with movieCharacters.MovieId = movieId
            var movieCharacters = _context.MovieCharacter.Where(mc => mc.MovieId == movie.MovieId);

            // Query DbSet<Character> for matches with movieCharacters.CharacterId
            var charactersInMovie = from character in _context.Characters
                        from movieCharacter in movieCharacters
                        where character.CharacterId == movieCharacter.CharacterId
                        select character;

            if (charactersInMovie.Count() == 0)
                return NotFound();

            return _mapper.Map<List<CharacterReadDTO>>(await charactersInMovie.ToListAsync());
        }

        /// <summary>
        /// Update a specific movie.
        /// If the franchise id is 0 leave it to autofill to null.
        /// </summary>
        /// <param name="id">Id for the movie to update.</param>
        /// <param name="movie">The movie to update as JSON.</param>
        /// <response code = "204">If a movie is successfully updated.</response>
        /// <response code = "404">If no movie with that id exists.</response>
        /// <response code = "400">If the movie id is not the same as the movie id in the JSON.</response>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movie)
        {
            if (id != movie.MovieId)
            {
                return BadRequest();
            }

            Movie domainMovie = _mapper.Map<Movie>(movie);
            if (domainMovie.FranchiseId == 0)
                domainMovie.FranchiseId = null;
            _context.Entry(domainMovie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add a new movie to the database.
        /// If the franchise id is 0 leave it to autofill to null.
        /// </summary>
        /// <param name="movie">The JSON for the movie to add.</param>
        /// <response code = "201">If a movie is successfully added.</response>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<MovieCreateDTO>> PostMovie(MovieCreateDTO movie)
        {
            Movie domainMovie = _mapper.Map<Movie>(movie);

            if(domainMovie.FranchiseId == 0)
                domainMovie.FranchiseId = null;

            _context.Movies.Add(domainMovie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = domainMovie.MovieId }, domainMovie);
        }

        /// <summary>
        /// Add a character to an existing movie.
        /// Submit franchise id.
        /// Submit character id.
        /// </summary>
        /// <param name="movieId">The id of the movie.</param>
        /// <param name="charId">The id of the character.</param>
        /// <response code = "204">If a character is successfully added to a movie.</response>
        /// <response code = "404">If no movie with that id exists. Or if no character with that id exists.</response>
        /// <response code = "400">If DbUpdateException is thrown.</response>
        /// <returns></returns>
        [HttpPost("{movieId}, {charId}")]
        public async Task<IActionResult> AddCharacterToMovie(int movieId, int charId)
        {
            var movie = await _context.Movies.FindAsync(movieId);
            var movChar = await _context.Characters.FindAsync(charId);
            if (movie == null || movChar == null)
                return NotFound();

            try
            {
                await _context.MovieCharacter.AddAsync(new() { CharacterId = charId, MovieId = movieId });
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }

            return NoContent();
        }

        /// <summary>
        /// Delete a movie from the database.
        /// Submit movie id.
        /// </summary>
        /// <param name="id"></param>
        /// <response code = "204">If a movie is successfully deleted from a movie.</response>
        /// <response code = "404">If no movie with that id exists.</response>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Delete character from movie.
        /// Submit the character id.
        /// Submit the movie id.
        /// </summary>
        /// <param name="charId">The id for the character.</param>
        /// <param name="movieId">The id for the movie.</param>
        /// <response code = "204">If a character is successfully deleted from a movie.</response>
        /// <response code = "404">If no character with that id exists. Or if no movie with that id exists.</response>
        /// <returns></returns>
        [HttpDelete("{charId}, {movieId}")]
        public async Task<IActionResult> DeleteCharacterFromMovie(int charId, int movieId)
        {
            var movie = await _context.Movies.FindAsync(movieId);
            var movChar = await _context.Characters.FindAsync(charId);
            if (movie == null || movChar == null)
                return NotFound();

            _context.MovieCharacter.Remove(new() { CharacterId = charId, MovieId = movieId });
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }
    }
}
