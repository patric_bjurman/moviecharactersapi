﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Character;
using System.Net.Mime;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieCharactersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all characters from the database.
        /// </summary>
        /// <response code = "200">When all characters are returned.</response>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _context.Characters.ToListAsync());
        }

        /// <summary>
        /// Get a specific character from the database.
        /// Submit character id.
        /// </summary>
        /// <param name="id">The id for the character.</param>
        /// <response code = "200">When a character are returned.</response>
        /// <response code = "404">When no character with that id exists.</response>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Update a specific character.
        /// </summary>
        /// <param name="id">The id for the character.</param>
        /// <param name="character">The character in JSON.</param>
        /// <response code = "204">If a character is successfully updated.</response>
        /// <response code = "404">If no character with that id exists.</response>
        /// <response code = "400">If the character id is not the same as the character id in the JSON.</response>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO character)
        {
            if (id != character.CharacterId)
            {
                return BadRequest();
            }

            Character domainCharacter = _mapper.Map<Character>(character);
            _context.Entry(domainCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add a new character to the database.
        /// </summary>
        /// <param name="character">The character in JSON.</param>
        /// <response code = "201">If a character is successfully added.</response>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CharacterCreateDTO>> PostCharacter(CharacterCreateDTO character)
        {
            Character domainChar = _mapper.Map<Character>(character);
            _context.Characters.Add(domainChar);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = domainChar.CharacterId }, domainChar);
        }

        /// <summary>
        /// Delete a character from the database.
        /// Submit character id.
        /// </summary>
        /// <param name="id">The id for the character.</param>
        /// <response code = "204">If a character is successfully deleted.</response>
        /// <response code = "404">If no character with that id exists.</response>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.CharacterId == id);
        }
    }
}
