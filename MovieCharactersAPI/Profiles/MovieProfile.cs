﻿using AutoMapper;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            // Movie<->MovieCreateDTO
            CreateMap<Movie, MovieCreateDTO>()
                .ReverseMap();
            // Movie<->MovieEditDTO
            CreateMap<Movie, MovieEditDTO>()
                .ReverseMap();
            // Movie<->MovieReadDTO
            CreateMap<Movie, MovieReadDTO>()
                .ReverseMap();
        }
    }
}
