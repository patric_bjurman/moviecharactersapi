﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models
{
    public class Character
    {
        // Pk
        public int CharacterId { get; set; }
        // Fields
        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        [MaxLength(255)]
        public string PictureUrl { get; set; }
        // Relationships
        public virtual ICollection<MovieCharacter> Movies { get; set; }
    }
}