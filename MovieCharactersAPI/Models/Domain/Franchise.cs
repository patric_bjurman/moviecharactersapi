﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models
{
    public class Franchise
    {
        // Pk
        public int FranchiseId { get; set; }
        // Fields
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(255)]
        public string Description { get; set; }
        // Relationships
        public virtual ICollection<Movie> Movies { get; set; }
    }
}